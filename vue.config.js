module.exports = {
  devServer: {
    host: 'moa01.gakkixmasami.com',
    proxy: {
      '/api': {
        target: 'http://jdnrsaengine.gakkixmasami.com',
        changeOrigin: true,
        pathRewrite: {
          '^/api': '',
        },
      },
    },
  },
}