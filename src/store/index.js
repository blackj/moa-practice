import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    apiToken: null,
    menu: null,
    timezone: 8,
    internalList: [],
    isLoading: false
  },
  mutations: {
    loginAuth (state, token) {
      state.apiToken = token
    },
    settingMenu (state, menuList) {
      state.menu = menuList
    },
    settingTimeZone(state, timezone) {
      state.timezone = timezone
      localStorage.setItem('timezone', state.timezone)
      // window.location.reload()
    },
    appendInternalList(state, id) {
      state.internalList.push(id)
    },
    isLoading(state, nowState) {
      state.isLoading = nowState
    }
  },
  actions: {
  },
  modules: {
  }
})
