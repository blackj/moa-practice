const menuListMap = {
  'accounts': {
    'shareholder': {
      path: 'shareholder',
      name: '股东管理'
    },
    'general_agents': {
      path: 'general_agents',
      name: '总代管理'
    },
    'agents': {
      path: 'agents',
      name: '内部代理管理'
    },
    'players': {
      path: 'players',
      name: '會員管理'
    },
    'player_registers': {
      path: 'player_registers',
      name: '會員注冊設定'
    },
    'agent_systems': {
      path: 'agent_systems',
      name: '代理体系查詢'
    },
    '会员等级管理': {
      path: '会员等级管理',
      name: '會員层級管理'
    },
    'hall': {
      path: 'hall',
      name: '廳主管理'
    },
    'hall_sub': {
      path: 'hall_sub',
      name: '廳主子帐号'
    },
    'auth_group': {
      path: 'auth_group',
      name: '權限群組'
    },
    '代理層級管理': {
      path: '代理層級管理',
      name: '代理层级管理'
    },
    '代理稽核': {
      path: '代理稽核',
      name: '代理稽核'
    },
    '外部代理管理': {
      path: '外部代理管理',
      name: '外部代理管理'
    },
    'levels': {
      path: 'levels',
      name: '快速晉級更新'
    }
  },
  'bulletins': {
    'bulletin': {
      path: 'bulletin',
      name: '公告管理'
    },
    'message': {
      path: 'message',
      name: '讯息管理'
    },
    'announcement': {
      path: 'announcement',
      name: '系统公告'
    }
  },
  'games': {
    'game': {
      path: 'game',
      name: '遊戲设定'
    },
    '即時注單': {
      path: '即時注單',
      name: 'mega彩票-即时注单'
    },
    'MEGA彩票风控设定': {
      path: 'MEGA彩票风控设定',
      name: 'mega彩票-風控设定'
    }
  },
  'moneys': {
    'fourth_deposits': {
      path: 'fourth_deposits',
      name: '網銀入款'
    },
    'provider_deposits': {
      path: 'provider_deposits',
      name: '轉帳入款'
    },
    'provider_withdraws': {
      path: 'provider_withdraws',
      name: '轉帳出款'
    },
    'payment_setting': {
      path: 'payment_setting',
      name: '支付設定'
    },
    'company_bank': {
      path: 'company_bank',
      name: '公司銀行卡管理'
    },
    '出款銀行設定': {
      path: '出款銀行設定',
      name: '出款銀行設定'
    },
    '會員資金調整': {
      path: '會員資金調整',
      name: '會員資金調整'
    }
  },
  'operatelogs': {
    '操作纪录': {
      path: '操作纪录',
      name: '操作纪录'
    }
  },
  'promotions': {
    "优惠活动紀錄": {
      path: "优惠活动紀錄",
      name: '优惠活动紀錄'
    },
    '優惠活動-手動派發': {
      path: '優惠活動-手動派發',
      name: '優惠活動-手動派發'
    },
    '優惠活動-自動派發': {
      path: '優惠活動-自動派發',
      name: '優惠活動-自動派發'
    },
    '優惠券': {
      path: '優惠券',
      name: '優惠券'
    },
    '簽到禮金': {
      path: '簽到禮金',
      name: '簽到禮金'
    },
    "优惠查询派发": {
      path: "优惠查询派发",
      name: "优惠查询派发"
    },
    "生日禮金": {
      path: "生日禮金",
      name: "生日禮金"
    },
    "红包抽抽乐": {
      path: "红包抽抽乐",
      name: "红包抽抽乐"
    },
    "救援金": {
      path: "救援金",
      name: "救援金"
    }
  },
  'rebate': {
    'rebate': {
      path: 'rebate',
      name: '返水派發'
    },
    'rebate_setting': {
      path: 'rebate_setting',
      name: '返水設定'
    }
  },
  'reports': {
    "account_summary": {
      path: "account_summary",
      name: '會員流水明細'
    },
    "game_bet_report": {
      path: "game_bet_report",
      name: '遊戲投注報表'
    },
    "出入款报表": {
      path: "出入款报表",
      name: "出入款报表"
    },
    "入款纪录表": {
      path: "入款纪录表",
      name: "入款纪录表"
    },
    "出款纪录表": {
      path: "出款纪录表",
      name: "出款纪录表"
    },
    "登入装置报表": {
      path: "登入装置报表",
      name: "登入装置报表"
    },
    "user_login_information": {
      path: "user_login_information",
      name: '會員IP列表'
    },
    "营收报表": {
      path: "营收报表",
      name: "营收报表"
    },
    "会员金流": {
      path: "会员金流",
      name: "会员金流"
    },
    "玩家输赢报表": {
      path: "玩家输赢报表",
      name: "玩家输赢报表"
    },
    "下线投注报表": {
      path: "下线投注报表",
      name: "下线投注报表"
    },
    "會員登入紀錄": {
      path: "會員登入紀錄",
      name: "會員登入紀錄"
    },
    "在線會員表": {
      path: "在線會員表",
      name: "在線會員表"
    },
    "會員流水明細": {
      path: "會員流水明細",
      name: "會員流水明細"
    },
    "出入款總匯": {
      path: "出入款總匯",
      name: "出入款總匯"
    },
    "資金明細": {
      path: "funds-reports",
      name: "資金報表"
    },
    "会员等级明细": {
      path: '会员等级明细',
      name: "会员等级明细"
    }
  },
  'rewards': {
    'promote_rebates': {
      path: 'promote_rebates',
      name: '推廣返佣'
    },
    'promotion_system': {
      path: 'promotion_system',
      name: '推廣查詢'
    },
    'promotion_setting': {
      path: 'promotion_setting',
      name: '推廣返佣設定'
    }
  },
  'sites': {
    'site': {
      path: 'site',
      name: '站點設置'
    },
    "首页广告管理": {
      path: "首页广告管理",
      name: "首页广告管理"
    },
    "优惠活动管理": {
      path: "优惠活动管理",
      name: "优惠活动管理"
    },
    "文案管理": {
      path: "文案管理",
      name: "文案管理"
    },
    "圖庫": {
      path: "圖庫",
      name: "圖庫"
    },
    "訪客": {
      path: "訪客",
      name: "訪客"
    },
    "熱門遊戲": {
      path: "熱門遊戲",
      name: "熱門遊戲"
    },
    "中獎名單": {
      path: "中獎名單",
      name: "中獎名單"
    }
  }
}

export {
  menuListMap
}