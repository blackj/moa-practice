import Vue from 'vue'
import App from './App.vue'
import axios from 'axios'
import VueAxios from 'vue-axios'
import global_ from './global'
import router from './router'
import store from './store'
import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';
import { library } from '@fortawesome/fontawesome-svg-core'
import { faBorderAll, faUsers, faYenSign, faUserPlus, faHandHoldingUsd
  , faBullhorn, faChartPie, faWindowMaximize, faGift, faList, faAngleDown
  , faIndent, faVolumeUp, faVolumeMute, faUser, faUserEdit, faSignOutAlt, faSpinner
  , faAngleLeft, faAngleRight} from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'

library.add(faBorderAll, faUsers, faYenSign, faUserPlus, faHandHoldingUsd, faBullhorn, faChartPie
  , faWindowMaximize, faGift, faList, faAngleDown, faIndent, faVolumeUp, faVolumeMute, faUser
  , faSignOutAlt, faUserEdit, faSpinner, faAngleRight, faAngleLeft)
 
Vue.component('font-awesome-icon', FontAwesomeIcon)
 
Vue.config.productionTip = false
Vue.prototype.GLOBAL = global_

Vue.use(VueAxios, axios)
Vue.use(ElementUI)
Vue.use(require('vue-moment'))

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
