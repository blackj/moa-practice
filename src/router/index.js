import Vue from 'vue'
import VueRouter from 'vue-router'
import LoginPage from '@/views/LoginPage.vue'
import Dashboard from '@/views/Dashboard.vue'
import FundsReports from '@/views/FundsReports.vue'

Vue.use(VueRouter)

  const routes = [
  {
    path: '/',
    name: 'LoginPage',
    component: LoginPage
  },
  {
    path: '/dashboard',
    name: 'Dashboard',
    component: Dashboard
  },
  {
    path:'/reports/funds-reports',
    name: 'FundsReports',
    component: FundsReports
  }
]

const router = new VueRouter({
  mode: 'history',
  routes
})

export default router
